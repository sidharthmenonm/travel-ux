import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store';
Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'Home',
    component: Home,
    meta: { permission: "true", title: "Dashboard" }
  },
  {
    path: '/login',
    name: 'Login',
    component: () =>
      import ('../views/travel/login.vue'),
    meta: { title: "Login" }
  },
  {
    path: '/order',
    name: 'Order',
    component: () =>
      import ('../views/travel/order.vue'),
    meta: { permission: "true", title: "Order Details" }
  },


]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {

  document.title = to.meta.title
  if (to.meta.permission) {
    store.dispatch('travel/checkauth').then((user) => {
        // console.log(to);
        next();
      })
      .catch((e) => {
        console.log(e, from);
        if (from.name == 'Login') {
          next({ name: 'nopermission' })
        } else {
          next({ name: 'Login' })
        }
      })
  } else {
    next();
  }
});

export default router