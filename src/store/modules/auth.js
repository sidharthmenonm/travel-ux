const state = {
  user: {

  },
  // errors: {},
  // sending: false,
};

const mutations = {
  // errors(state, errors) {
  //   state.errors = errors;
  // },
  // clear_errors(state) {
  //   state.errors = [];
  // },
  // start_sending(state) {
  //   state.sending = true;
  // },
  // stop_sending(state) {
  //   state.sending = false;
  // },
  save_user(state, user) {
    state.user = user;
  }
};

const actions = {
  can(context, permission) {
    return state.user.user.permissions.includes(permission) ? true : false;
  },
  checkauth(context, permission) {
    return new Promise((resolve, reject) => {
      if (state.user.user.permissions.includes(permission)) {
        resolve(state.user);
      } else {
        reject();
      }
    })
  },
  set_token(context, { _vm, token }) {
    _vm.$http.defaults.headers.common['Authorization'] = 'Bearer ' + token
  },
  login(context, _vm) {
    context.dispatch('api/call', { _vm: _vm, endpoint: 'login', data: _vm.form, method: 'POST' }, { root: true })
      .then((data) => {
        context.commit('save_user', data);
        context.dispatch('set_token', { _vm, token: data.token });
        if (_vm.form.remember) {
          localStorage.setItem('user', JSON.stringify(data));
        }
        _vm.$router.push({ name: 'Home' });
      })
  },
  load_user(context, _vm) {
    return new Promise((resolve, reject) => {
      if (localStorage.getItem('user')) {
        try {
          var data = JSON.parse(localStorage.getItem('user'))
          context.commit('save_user', data)
          context.dispatch('set_token', { _vm, token: data.token });
          _vm.$router.push('/');
        } catch (e) {
          localStorage.removeItem('user');
        }
      }
    });
  },
  logout(context, _vm) {
    localStorage.removeItem('user');
    context.commit('save_user', {});
    _vm.$router.push('/login');
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
}