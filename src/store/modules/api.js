const state = {
  errors: {},
  sending: false,
}

const mutations = {
  errors(state, errors) {
    state.errors = errors;
  },
  clear_errors(state) {
    state.errors = [];
  },
  start_sending(state) {
    state.sending = true;
  },
  stop_sending(state) {
    state.sending = false;
  },
}

const actions = {
  call(context, { _vm, endpoint, method, data, params }) {
    context.commit('start_sending');
    context.commit('clear_errors');
    return new Promise((resolve, reject) => {
      _vm.$http({
        url: endpoint,
        method,
        data,
        params
      }).then((response) => {
        context.commit('stop_sending');
        context.dispatch('toast/add_message', response.data.message, { root: true })
        resolve(response.data);
      }).catch((error) => {
        context.commit('errors', error.response.data);
        context.dispatch('toast/add_message', error.response.data.message, { root: true })
        context.commit('stop_sending');
        // reject(error);
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}