const state = {
  open: false
}

const mutations = {
  change_menu(state) {
    state.open = !state.open;
  }
}

const actions = {
  toggle_menu(context) {
    context.commit('change_menu');
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}