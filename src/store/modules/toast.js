const state = {
  toasts: []
}

const mutations = {
  add_message(state, data) {
    state.toasts.push(data)
  }
}

const actions = {
  add_message(context, message) {
    context.commit('add_message', {
      message: message,
      dismiss: false
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}