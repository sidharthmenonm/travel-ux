const state = {
  travel: [],
  da: [],
  user: {},
  order: {}
}

const mutations = {
  save_user(state, user) {
    state.user = user;
  },
  save_order(state, order) {
    state.order = order;
  },
  open_file(state, data) {
    state.user = data.user;
    state.order = data.order;
    state.travel = data.travel;
    state.da = data.da;
  }
}

const actions = {
  open_file(context, { vm, data }) {
    context.commit('open_file', data);
    vm.$router.push({ 'name': 'Home' });
  },
  save_user(context, user) {
    context.commit('save_user', user);
  },
  store_order(context, order) {
    context.commit('save_order', order);
  },
  save_order(context, { vm }) {
    context.commit('save_order', vm.order);
    vm.$router.push({ 'name': 'Home' });
  },
  checkorder(context) {
    console.log(JSON.stringify(state.order), state.order.number, state.order.length);
    return new Promise((resolve, reject) => {
      if (state.order.number) {
        resolve(state.order);
      } else {
        reject();
      }
    })
  },
  checkauth(context) {
    console.log(state.user);
    return new Promise((resolve, reject) => {
      if (state.user.name) {
        resolve(state.user);
      } else {
        reject();
      }
    })
  },
  load_user(context) {
    return new Promise((resolve, reject) => {
      if (localStorage.getItem('user')) {
        try {
          var data = JSON.parse(localStorage.getItem('user'));
          context.commit('save_user', data);
          resolve();
        } catch (e) {
          console.log(e);
          reject();
        }
      }
    });
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}