import Vue from 'vue'
import App from './App.vue'
import Axios from 'axios'
import './registerServiceWorker'
import router from './router'
import store from './store'
import VueMask from 'v-mask'
import VTooltip from 'v-tooltip'
Vue.use(require('vue-moment'));

import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

window.pdfMake = pdfMake;
window.$ = window.jQuery = require('jquery');
import 'bootstrap'

Vue.config.productionTip = false

Axios.defaults.baseURL = "http://meet-api.local"

Vue.prototype.$http = Axios;
Vue.use(VueMask)
Vue.use(VTooltip)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')